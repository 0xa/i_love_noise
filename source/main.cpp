/*-----------------------------------------------------------------------------

 I <3 NOISE  I <3 NOISE  I <3 NOISE  I <3 NOISE  I <3 NOISE  I <3 NOISE  I DO

 (C) Copyright 2010 - Aymeric Mansoux

 This program is free software. It comes without any warranty, to the extent
 permitted by applicable law. You can redistribute it and/or modify it under 
 the terms of the Do What The Fuck You Want To Public License, Version 2, as
 published by Sam Hocevar. See http://sam.zoy.org/wtfpl/COPYING for more
 details.

-----------------------------------------------------------------------------*/

#include <nds.h>
#include <stdio.h>
#include <math.h>

#define abs(x)    (((x)<0)?(-(x)):(x))
#define min(x, y) (((x)<(y))?(x):(y))

int x = 0;
int y = 0;
int size[3];

int colors[2] = {RGB15(31,31,31),RGB15(0,0,0)};
int color_index[3] = {0,0,0};

u16* vram[4];
u32 mode[3];

u16* sound_buffer[3];
int pitch[3];
int chan[3];
int bank = 0;

// dmaCopySafe --------------------------------------------------------------
// http://www.coranac.com/2009/05/dma-vs-arm9-fight/

#define REG_DMA3CNT     (*(vu32*)0x40000DC)

bool dmaCopySafe(const void *src, void *dst, u32 size)
{
  u32 srca= (u32)src, dsta= (u32)dst;

  // Check TCMs and BIOS (0x01000000, 0x0B000000, 0xFFFF0000).
  //# NOTE: probably incomplete checks.
  if((srca>>24)==0x01 || (srca>>24)==0x0B || (srca>>24)==0xFF)
    return false;
  if((dsta>>24)==0x01 || (dsta>>24)==0x0B || (dsta>>24)==0xFF)
    return false;

  if((srca|dsta) & 1)                   // Fail on byte copy.
    return false;

  while(REG_DMA3CNT & DMA_BUSY) ;

  if((srca>>24)==0x02)                // Write cache back to memory.
    DC_FlushRange(src, size);

  if((srca|dsta|size) & 3)
    dmaCopyHalfWords(3, src, dst, size);
  else
    dmaCopyWords(3, src, dst, size);

  if((dsta>>24)==0x02)                // Set cache of dst range to 'dirty'
    DC_InvalidateRange(dst, size);

  return true;
}

//---------------------------------------------------------------------------

void Vblank()
{
  // Is it a good place?
  scanKeys();
}
	
void ClearScreen(int bank)
{
  for(int i = 0; i < 256 * 192; i++)
    vram[bank][i] = RGB15(0,0,0);
}

void DrawRect(int x, int y, int width, int height, u16* buffer)
{
  buffer += y * SCREEN_WIDTH + x;
  for(int i = 0; i < height; ++i)
    {
      u16* line = buffer + (SCREEN_WIDTH * i);
      for(int j = 0; j < width; ++j) 
	{
	  *line++ = colors[color_index[bank]];
	}
    }	
}

int X[3] = {0,0,0};
int Y[3] = {0,0,0};
int XW[3] = {1,1,1};
int YW[3] = {1,1,1};
int signX[3] = {1,1,1};
int signY[3] = {-1,-1,-1};

void cabinet(int bank)
{
      X[bank] = (XW[bank] * signX[bank] +X[bank])%SCREEN_WIDTH; 
      Y[bank] = (YW[bank] * signY[bank] +Y[bank])%SCREEN_HEIGHT;

      if (X[bank] < 0)
	{
	  X[bank] = SCREEN_WIDTH - ( X[bank] * -1 );
	  color_index[bank] = (color_index[bank] + 1)%2 ;
	}
      if (Y[bank] < 0)
	{
	  Y[bank] = SCREEN_HEIGHT - ( Y[bank] * -1 );
	  color_index[bank] = (color_index[bank] + 1)%2 ;
	}

      /* draw a square */
      DrawRect(X[bank], Y[bank], abs(XW[bank]), abs(YW[bank]), vram[bank]);

      // send to sound buffer
      dmaCopySafe(vram[bank], sound_buffer[bank], 98332);
}

void rand_pattern(int bank)
{
  X[bank] = 0;
  Y[bank] = 0;
  XW[bank] = rand()%32+1;
  YW[bank] = rand()%32+1;
  signX[bank] = signX[bank] * -1;
  signY[bank] = signY[bank] * -1;
}


int32 f = 40;
int32 rate = 40;


void consoleRefresh()
{
  consoleClear();
  iprintf("\x1b[31m\x1b[00:0H                            v0.2\x1b[39m\n");
  iprintf("\e[03:0H      ____ ____  ______\n");
  iprintf("\e[04:0H     /    |    \\/   _  \\\n");
  iprintf("\e[05:0H     \\   _|_       /_\\  \\\n");
  iprintf("\e[06:0H      \\  \\_/        |    \\\n");
  iprintf("\e[07:0H       \\______/\\____|____/\n");
  iprintf("\e[09:0H           I \x1b[31;1m<3\x1b[39m Noise\n");
  iprintf("\x1b[37m\x1b[15;0H            %d Hz",pitch[bank]);
  iprintf("\x1b[16;0H             %d px",size[bank]);
  iprintf("\x1b[17;0H             BANK %d",bank);
  iprintf("\x1b[18;0H             X %d",X[bank]);
  iprintf("\x1b[19;0H             Y %d",Y[bank]);
  iprintf("\x1b[20;0H             XW %d",XW[bank]);
  iprintf("\x1b[21;0H             YW %d",YW[bank]);
}

int main(void) {

  powerOn(POWER_ALL_2D);

  // Initialize the screens
  videoSetMode(MODE_FB0);
  mode[0] = MODE_FB0;
  mode[1] = MODE_FB1;
  mode[2] = MODE_FB3;
  vramSetBankA(VRAM_A_LCD);	
  vramSetBankB(VRAM_B_LCD);
  vramSetBankD(VRAM_D_LCD);
  vram[0] = VRAM_A;
  vram[1] = VRAM_B;
  vram[2] = VRAM_D;
  vram[3] = VRAM_C;
  lcdSwap();
  irqSet(IRQ_VBLANK, Vblank);

  // Console ------------------------------------------------------------------
  // This is fucked up ATM, but will please the glitch lover in you...
  PrintConsole cons;
  videoSetModeSub(MODE_5_2D | DISPLAY_BG1_ACTIVE);
  vramSetBankC(VRAM_C_SUB_BG);
  dmaCopySafe(vram[0], VRAM_C, 98332);
  consoleInit(&cons, 0,BgType_Text4bpp, BgSize_T_256x256, 31, 0, false, true);
  consoleSelect(&cons);
  bgSetPriority(cons.bgId, 1);
  
  REG_BG1CNT_SUB = BG_BMP16_256x256 | BG_BMP_BASE(0) | BG_PRIORITY(3); 

  REG_BG2PA_SUB = 1 << 8;
  REG_BG2PB_SUB = 0;
  REG_BG2PC_SUB = 0;
  REG_BG2PD_SUB = 1 << 8;

  REG_BG2X_SUB = 0;
  REG_BG2Y_SUB = 0;

  //fontBgMap = BG_MAP_RAM(20 + 32);
  //---------------------------------------------------------------------------

  soundEnable();
  touchPosition touchXY;
  for (int i = 0; i <= 2; i++)
    {
      // pencil size
      size[i] = 4;
      // sound buffer init
      sound_buffer[i] = (u16*)malloc(98332);
      pitch[i] = 11111 + (11111 * i);
      chan[i] = soundPlaySample(sound_buffer[i],
				SoundFormat_16Bit, 
				98304, 
				pitch[i], 
				127,
				(i * 63), // ahem...
				true, 
				0);
      // clear vram and copy to sound buffer
      ClearScreen(i);
      dmaCopySafe(vram[i], sound_buffer[i], 98332);
    }
  
  while(1) 
    {
      swiWaitForVBlank();
      touchRead(&touchXY);

      consoleRefresh();
      cabinet(0);
      cabinet(1);
      cabinet(2);

      // Restart
      if (keysDown() & KEY_R)
	{
	  ClearScreen(bank);
	  dmaCopySafe(vram[bank], sound_buffer[bank], 98332);
	}
      if (keysDown() & KEY_B)
	{
	  color_index[bank] = (color_index[bank] + 1)%2 ;
	}

      // Rand settings
      if (keysDown() & KEY_L)
	{
	  ClearScreen(bank);
	  rand_pattern(bank);
	  consoleRefresh();
	}
      if (keysUp() & KEY_L)
	{
	  dmaCopySafe(sound_buffer[bank], vram[bank], 98332);
	}
      // rate and co
      if (keysHeld() & KEY_L)
	{
	  if (keysDown() & KEY_UP)
	    f += 1;
	  if (keysDown() & KEY_DOWN)
	    f -= 1;
	  if (keysDown() & KEY_LEFT)
	    rate -= 1;
	  if (keysDown() & KEY_RIGHT)
	    rate += 1;
	  consoleRefresh();
	}

      // size and buffer selection
      if (keysDown() & KEY_LEFT)
	{
	  size[bank] -= 1;
	  consoleRefresh();
	} else if (keysDown() & KEY_RIGHT)
	{
	  size[bank] += 1;
	  consoleRefresh();
	} else if (keysDown() & KEY_A)
        {
	  bank = (bank + 1)%3;
	  videoSetMode(mode[bank]);
	  consoleRefresh();
          
        } 
      // pitch
      if ((keysDown() | keysHeld()) & KEY_UP)
	{
	  pitch[bank] += 50;
	  consoleRefresh();
	  soundSetFreq(chan[bank], pitch[bank]);
	} else if ((keysDown() | keysHeld()) & KEY_DOWN)
	{
	  pitch[bank] -= 50;
	  consoleRefresh();
	  soundSetFreq(chan[bank], pitch[bank]);
	}	
      
      // Drag and scale
      if (keysDown() & KEY_TOUCH)
	{
	  x = touchXY.px;
          y = touchXY.py;
	}
      if (keysHeld() & KEY_TOUCH)
	{
	  XW[bank] = min( XW[bank] + ( touchXY.px - x ), SCREEN_WIDTH );
          YW[bank] = min( YW[bank] + ( touchXY.py - y ), SCREEN_HEIGHT );
	  
	  x = touchXY.px;
          y = touchXY.py;
        }
	
    }
  
  return 0;
}
